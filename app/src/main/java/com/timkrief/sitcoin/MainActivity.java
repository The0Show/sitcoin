/**
 *         SitCoin is a personal currency to balance the time you spend moving
 *         and the time you spend sitting.
 *         Copyright (C) 2021 Tim Krief
 *
 *         This program is free software: you can redistribute it and/or modify
 *         it under the terms of the GNU General Public License as published by
 *         the Free Software Foundation, either version 3 of the License, or
 *         (at your option) any later version.
 *
 *         This program is distributed in the hope that it will be useful,
 *         but WITHOUT ANY WARRANTY; without even the implied warranty of
 *         MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *         GNU General Public License for more details.
 *
 *         You should have received a copy of the GNU General Public License
 *         along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.timkrief.sitcoin;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.tabs.TabLayout;

import androidx.viewpager.widget.ViewPager;
import androidx.appcompat.app.AppCompatActivity;

import android.provider.AlarmClock;
import android.view.View;
import android.widget.TextView;

import com.timkrief.sitcoin.ui.main.SectionsPagerAdapter;
import com.timkrief.sitcoin.databinding.ActivityMainBinding;

public class MainActivity extends AppCompatActivity {

    public float previousActionAmount;
    public long previousActionTime;
    public float earningRate;
    private float fakeSitcoinAmount;

    private ActivityMainBinding binding;

    FloatingActionButton fab;
    TextView sitcoinUnits;
    TextView sitcoinDecimals;

    Intent timer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        SharedPreferences sharedPref = getPreferences(Context.MODE_PRIVATE);

        previousActionAmount = sharedPref.getFloat("previousActionAmount", 0.0f);
        previousActionTime = sharedPref.getLong("previousActionTime", System.currentTimeMillis());
        earningRate = sharedPref.getFloat("earningRate", 0.0f);

        binding = ActivityMainBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        SectionsPagerAdapter sectionsPagerAdapter = new SectionsPagerAdapter(this, getSupportFragmentManager());
        ViewPager viewPager = binding.viewPager;
        viewPager.setAdapter(sectionsPagerAdapter);
        TabLayout tabs = binding.tabs;
        tabs.setupWithViewPager(viewPager);
        fab = binding.fab;

        if(earningRate != 0) {
            fab.setImageResource(android.R.drawable.ic_media_pause);
        }
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(earningRate != 0){
                    fab.setImageResource(android.R.drawable.ic_media_play);
                    updateEarning(0);
                }
                else{
                    fab.setImageResource(android.R.drawable.ic_media_pause);
                    if(tabs.getSelectedTabPosition() == 0){
                        updateEarning(5);
                    }
                    if(tabs.getSelectedTabPosition() == 1){
                        updateEarning(-1);

                        timer = new Intent(AlarmClock.ACTION_SET_TIMER);
                        timer.putExtra(AlarmClock.EXTRA_LENGTH, (int)(sitcoinAmount() * 60.0f));
                        timer.putExtra(AlarmClock.EXTRA_MESSAGE, "SitCoins");
                        timer.putExtra(AlarmClock.EXTRA_SKIP_UI, true);
                        startActivity(timer);

                    }
                }
            }
        });

        sitcoinUnits = binding.units;
        sitcoinDecimals = binding.decimals;
        fakeSitcoinAmount = sitcoinAmount();
        updateCoinLabel(fakeSitcoinAmount);

        final Handler handler
                = new Handler();

        handler.post(new Runnable() {
            private int counter = 0;
            @Override

            public void run()
            {
                if (earningRate != 0){

                    if(fakeSitcoinAmount<=0) {
                        fakeSitcoinAmount = sitcoinAmount();
                    }
                    if(fakeSitcoinAmount<=0) {
                        fab.setImageResource(android.R.drawable.ic_media_play);
                        updateEarning(0);
                        updateCoinLabel(0);
                        handler.postDelayed(this, 500);
                    } else {
                        updateCoinLabel(fakeSitcoinAmount);
                        fakeSitcoinAmount += 0.01 * Math.signum(earningRate);
                        handler.postDelayed(this, (int)(600.f/Math.abs(earningRate)));
                    }
                }
                else {
                    handler.postDelayed(this, 500);
                }

            }
        });

    }
    private void updateCoinLabel(float amount){
        int units = (int) amount;
        sitcoinUnits.setText(Integer.toString(units));

        int decimals = (int) (amount * 100) - units * 100;
        sitcoinDecimals.setText(String.format("%02d", decimals));
    }

    public void updateEarning(float rate) {
        previousActionAmount = sitcoinAmount();
        if(previousActionAmount < 0){
            previousActionAmount = 0.0f;
        }
        previousActionTime = System.currentTimeMillis();
        earningRate = rate;

        SharedPreferences sharedPref = getPreferences(Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();

        editor.putFloat("previousActionAmount", previousActionAmount);
        editor.putLong("previousActionTime", previousActionTime);
        editor.putFloat("earningRate", earningRate);
        editor.apply();

        fakeSitcoinAmount = previousActionAmount;
        updateCoinLabel(fakeSitcoinAmount);
    }

    public float sitcoinAmount() {
        int elapsedTime = (int) (System.currentTimeMillis() - previousActionTime);
        return previousActionAmount + elapsedTime * earningRate / 60000.0f;
    }
}
